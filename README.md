# Price Rule Web UI using bpmn-js

An example of a price rule construction using bpmn-js.

![Screenshot](docs/screenshot.png)

## Run the Example

You need a [NodeJS](http://nodejs.org) development stack with [npm](https://npmjs.org) installed to build the project.

To build the example into the `public` folder execute

```sh
npm run build
```

To start the example execute

```sh
npm start
```